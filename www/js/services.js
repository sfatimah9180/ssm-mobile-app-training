angular.module('myServices', [])

.constant('config', {
    apiUrl: /*'http://192.168.1.129:8888/api/'//*/'http://128.199.251.250:8883/api/'
})

.factory('licInfo', function($ionicPopup, $interval) {
  var stop;
  function startInfo() {
    //alert("Interval");
    stop = $interval(showLicInfo, 20000);
  };

  function showLicInfo() {
    $interval.cancel(stop);
    var alertPopup = $ionicPopup.alert({
      title: '<h4 class="title-assertive">Demo Version App</h4>',
      template: "This version is for internal demo only",
      buttons: [
        { text: '<b class="title-class">Close</b>',
          type: 'button-positive button-small',
          onTap: function(e) {
            alertPopup.close();
            startInfo();
          }
        }
      ]
    });
  };

  return {
    start : startInfo
  }
})

.factory('myFmFactory', function() {
  
  var service = {};
  service.getButtons = function(lang) {
    var buttons = [{
    label: lang.MENU_08,
    icon: 'icon-icon2_308',
    dest: 'app.status308'
  },{
    label: lang.MENU_06,
    icon: 'icon-icon2_eC',
    dest: 'app.ecompound'
  },{
    label: lang.MENU_05,
    icon: 'icon-icon2_eQ',
    dest: 'app.equery'
  },{
    label: lang.MENU_07,
    icon: 'icon-icon2_eS',
    dest: 'app.esearch'
  }];
    
    return buttons;
  }
  return service;
})

.factory('myContactUs', function() {
  var contactlist = [{
    // label: 'Like Us on Facebook',
    icon: 'ion-social-facebook',
    dest: 'https://www.facebook.com/ssmofficialpage',
    mood: 'positive'
  },
  {
    // label: 'Follow Us on Twitter',
    icon: 'ion-social-twitter',
    dest: 'https://twitter.com/ssmofficialpage',
    mood: 'calm'
  },
    {
    // label: 'Follow Us on Instagram',
    icon: 'ion-social-instagram',
    dest: 'https://instagram.com/ssmofficialpage',
    mood: 'dark'
  },
  {
    // label: 'Subscribe our Channel on Youtube',
    icon: 'ion-social-youtube',
    dest: 'http://www.youtube.com/user/ssmofficialpage',
    mood: 'assertive'
  },
  {
    // label: 'Email Us: enquiry@ssm.com.my',
    // icon: 'ion-email',
    // dest: 'mailto:enquiry@ssm.com.my',
    // mood: 'dark'
  },
  {
    // label: 'Call Us: 03 2299 4400',
    // icon: 'ion-ios-telephone',
    // dest: 'tel:03-2299-4400',
    // mood: 'positive'
  }
  ];

  var service = {};
  service.getcontactlist = function() {
    return contactlist;
  }
  return service;
})

// only one for all queries because can only ask one per time
.factory('eQuerySvc', function($ionicPopup,langSvc) {
  var queryData = {
    first: "",
    second: "",
    query: ""
  };

  var setData = function(data) {
    queryData = data;
  };

  var getData = function() {
    return queryData;
  };
  var emptySearch = function() {
    
    var lang = translations[langSvc.getLang()];
    var alertPopup = $ionicPopup.alert({
      title: lang.ATTENTION,
      template: '<center><div translate="emptySearch"></div></center>',
      buttons: [
        { text: '<b class="title-class">Ok</b>',
          type: 'button-positive',
        }
      ]
    });
  }

  return {
    getData : getData,
    setData : setData,
    emptySearch :  emptySearch
  };
})

.factory('deviceAuth', function($http, $ionicHistory, $ionicLoading, $ionicPopup,config) {

  var uuid = "";
  var platform = "";
  var token = "";

  function setUUID(val) { uuid = val; }

  function setPlatform(val) { platform = val; }

  function setToken(val) { token = val; }

  function getDevInfo() {
    return {
      "uuid" : uuid,
      "platform" : platform,
      "token" : token
    }
  }

  var loadingShow = function() {
    $ionicLoading.show({
      template: '<p translate="SEARCHING">Searching ...</p><ion-spinner></ion-spinner>'
    });
  };

  var loadingHide = function(){
    $ionicLoading.hide();
  };

  function registerDevice() {
    loadingShow();
    var postUsers = $http({
      method: 'POST',
      //url : 'http://jsonplaceholder.typicode.com/posts/1/comments/',
      url: config.apiUrl + 'register-device',
      data: { "uuid" : uuid, "type" : platform }
    }).success(function(result) {
        return result.data;
    }).error(function(data, status) {
      // Do something on error
        console.log(data);
        var alertPopup = $ionicPopup.alert({
          title: '<h3 class="title-blue">Device Registration</h3>',
          template: status,
          buttons: [
            { text: '<b class="title-class">Close</b>',
              type: 'button-positive',
              onTap: function(e) {
                alertPopup.close();
              }
            }
          ]
        });
    }).finally(function() {
      // On both cases hide the loading
      loadingHide();
    });
    return postUsers;
  };

  return {
    setUUID : setUUID,
    setPlatform : setPlatform,
    registerDevice : registerDevice,
    getDevInfo : getDevInfo,
    setToken : setToken
  }
})

.factory('newsSvc', function($http, $ionicPopup, $ionicLoading, $ionicHistory, eQuerySvc, langSvc,config) {
    
  var loadingShow = function() {
    $ionicLoading.show({
      template: '<p translate="SEARCHING">Searching ...</p><ion-spinner></ion-spinner>'
    });
  };

  var loadingHide = function(){
    $ionicLoading.hide();
  };

  function getNews(title) {
    loadingShow();
    var lang = langSvc.getLang();
    var urlFinal = config.apiUrl + 'news/'+lang;
    //alert(urlFinal);

    var postUsers = $http({
      method: 'GET',
      url: urlFinal
    }).success(function(result) {
        return result.data;
    }).error(function(data, status) {
      // Do something on error
        var alertPopup = $ionicPopup.alert({
          title: title ,
          template: "{{'ERROR_QUERY'|translate}}"+status+"]",
          buttons: [
            { text: '<b class="title-class">Close</b>',
              type: 'button-positive',
              onTap: function(e) {
                alertPopup.close();
              }
            }
          ]
        });
    }).finally(function() {
      // On both cases hide the loading
      loadingHide();
    });
    return postUsers;
  };

  function getDetailNews() {
    var queryData = eQuerySvc.getData();
    //alert(queryData.query);
    loadingShow();
    var postUsers = $http({
      method: 'GET',
      url: queryData.query
    }).success(function(result) {
        return result.data;
    }).error(function(data, status) {
      // Do something on error
        var alertPopup = $ionicPopup.alert({
          title: title,
          template: "{{'ERROR_QUERY'|translate}}"+status+"]",
          buttons: [
            { text: '<b class="title-class">Close</b>',
              type: 'button-positive',
              onTap: function(e) {
                alertPopup.close();
              }
            }
          ]
        });
    }).finally(function() {
      // On both cases hide the loading
      loadingHide();
    });
    return postUsers;
  }

  return {
    getNews : getNews,
    getDetailNews : getDetailNews
  }
})

.factory('getQuery', function($http, $ionicPopup, $ionicLoading, $ionicHistory, deviceAuth, eQuerySvc, langSvc,config) {

  var resultData;
    
  var loadingShow = function() {
    $ionicLoading.show({
      template: '<p translate="SEARCHING">Searching ...</p><ion-spinner></ion-spinner>'
    });
  };

  var loadingHide = function(){
    $ionicLoading.hide();
  };

  var service = {};
  function loadUserData(title) {

    var devInfo = deviceAuth.getDevInfo();
    var queryData = eQuerySvc.getData();
    var outLang = langSvc.getLang();
    //alert(devInfo.token + " " + queryData.query);
    loadingShow();
    var postUsers = $http({
      //method: 'GET',
      method: 'POST',
      //url: 'http://jsonplaceholder.typicode.com/posts/1/comments/'
      url: config.apiUrl + 'equery',
      data: { "token" : devInfo.token, "documentNo" : queryData.query, lang : outLang }
    }).success(function(result) {
        //alert(result.data);
        if (result.data.length === 0) {
          var alertPopup = $ionicPopup.alert({
            title: title,
            template: '<center><div translate="norecord"></div></center>',
            buttons: [
              { text: '<b class="title-class">OK</b>',
                type: 'button-positive',
                onTap: function(e) {
//                  $ionicHistory.goBack();
                }
              }
            ]
          });
        }
        resultData = result.data;
        return result.data;
    }).error(function(data, status) {
        //alert(data + " " + status);
        var alertPopup = $ionicPopup.alert({
          title: title,
          template: "{{'ERROR_QUERY'|translate}}"+status+"]",
          buttons: [
            { text: '<b class="title-class">OK</b>',
              type: 'button-positive',
              onTap: function(e) {
//                $ionicHistory.goBack();
              }
            }
          ]
        });
    }).finally(function() {
      // On both cases hide the loading
      loadingHide();
    });
    return postUsers;
  };
    
  function getData(){
    return resultData;
  };

  
  return {
    loadUserData : loadUserData,
    getData: getData
  }
  
})

.factory('getCmpnd', function($http, $ionicPopup, $ionicLoading, $ionicHistory, deviceAuth, eQuerySvc,config) {
    
  var resultData;

  var loadingShow = function() {
    $ionicLoading.show({
      template: '<p translate="SEARCHING">Searching ...</p><ion-spinner></ion-spinner>'
    });
  };

  var loadingHide = function(){
    $ionicLoading.hide();
  };

  function loadUserData(title) {

    var devInfo = deviceAuth.getDevInfo();
    var queryData = eQuerySvc.getData();
    //alert(devInfo.token + " " +queryData.first + " " + queryData.second + " " + queryData.query);
    loadingShow();
    var postUsers = $http({
      method: 'POST',
      //url: 'http://jsonplaceholder.typicode.com/posts/1/comments/'
      url: config.apiUrl + 'ecompound',
      data: { "token" : devInfo.token, "type": queryData.first,
              "entityType": queryData.second, "entityNo": queryData.query }
    }).success(function(result) {
      if (result.data.length === 0) {
        var alertPopup = $ionicPopup.alert({
          title: title,
          template: '<center><div translate="norecord"></div></center>',
          buttons: [
            { text: '<b class="title-class">OK</b>',
              type: 'button-positive',
              onTap: function(e) {
//                $ionicHistory.goBack();
              }
            }
          ]
        });
      }
        resultData = result.data;
        return result.data;
    }).error(function(data, status) {
      // Do something on error
        var alertPopup = $ionicPopup.alert({
          title:  title,
          template: "{{'ERROR_QUERY'|translate}}"+status+"]",
          buttons: [
            { text: '<b class="title-class">OK</b>',
              type: 'button-positive',
              onTap: function(e) {
//                $ionicHistory.goBack();
              }
            }
          ]
        });
    }).finally(function() {
      // On both cases hide the loading
      loadingHide();
    });
    return postUsers;
  };


  function getData(){
    return resultData;
  };
    
  return {
    loadUserData : loadUserData,
    getData: getData
  }
})

.factory('getSearch', function($http, $ionicPopup, $ionicLoading, $ionicHistory, deviceAuth, eQuerySvc,config) {

  var resultData;
    
  var loadingShow = function() {
    $ionicLoading.show({
      template: '<p translate="SEARCHING">Searching ...</p><ion-spinner></ion-spinner>'
    });
  };

  var loadingHide = function(){
    $ionicLoading.hide();
  };

  function loadUserData(title) {

    var devInfo = deviceAuth.getDevInfo();
    var queryData = eQuerySvc.getData();
    console.log(devInfo.token + " : " +queryData.first + " : " + queryData.query);
    loadingShow();
    var postUsers = $http({
      method: 'POST',
      //url: 'http://jsonplaceholder.typicode.com/posts/1/comments/'
      url: config.apiUrl + 'esearch',
      data: { "token" : devInfo.token, "type": queryData.first, "value": queryData.query }
    }).success(function(result) {
        if (result.data.length === 0) {
          var alertPopup = $ionicPopup.alert({
            title: title,
            template: '<center><div translate="norecord"></div></center>',
            buttons: [
              { text: '<b class="title-class">OK</b>',
                type: 'button-positive',
                onTap: function(e) {
//                  $ionicHistory.goBack();
                }
              }
            ]
          });
        }
        resultData = result.data;
        return result.data;
    }).error(function(data, status) {
      // Do something on error
        var alertPopup = $ionicPopup.alert({
          title: title,
          template: "{{'ERROR_QUERY'|translate}}"+status+"]",
          buttons: [
            { text: '<b class="title-class">Back</b>',
              type: 'button-positive',
              onTap: function(e) {
//                $ionicHistory.goBack();
              }
            }
          ]
        });
    }).finally(function() {
      // On both cases hide the loading
      loadingHide();
    });
    return postUsers;
  };
  
  function getData(){
    return resultData;
  };

  return {
    loadUserData : loadUserData,
    getData: getData
  }
})

.factory('getS308', function($http, $ionicPopup, $ionicLoading, $ionicHistory, deviceAuth, eQuerySvc, config) {

  var resultData;

  var loadingShow = function() {
    $ionicLoading.show({
      template: '<p translate="SEARCHING">Searching ...</p><ion-spinner></ion-spinner>'
    });
  };

  var loadingHide = function(){
    $ionicLoading.hide();
  };

  function loadUserData(title) {

    var devInfo = deviceAuth.getDevInfo();
    var queryData = eQuerySvc.getData();
    //alert(devInfo.token + " " + queryData.query);
    loadingShow();
    var postUsers = $http({
      method: 'POST',
      //url: 'http://jsonplaceholder.typicode.com/posts/1/comments/'
      url: config.apiUrl + 'strikeoff',
      data: { "token" : devInfo.token, "companyNo": queryData.query }
    }).success(function(result) {
        if (result.data.length === 0) {
          var alertPopup = $ionicPopup.alert({
            title: title,
            template: '<center><div translate="norecord"></div></center>',
            buttons: [
              { text: '<b class="title-class">OK</b>',
                type: 'button-positive',
                onTap: function(e) {
//                  $ionicHistory.goBack();
                }
              }
            ]
          });
        }
        
        resultData = result.data;
        return result.data;
        
    }).error(function(data, status) {
      // Do something on error
        var alertPopup = $ionicPopup.alert({
          title: title,
          template: "{{'ERROR_QUERY'|translate}}"+status+"]",
          buttons: [
            { text: '<b class="title-class">OK</b>',
              type: 'button-positive',
              onTap: function(e) {
//                $ionicHistory.goBack();
              }
            }
          ]
        });
    }).finally(function() {
      // On both cases hide the loading
      loadingHide();
    });
    return postUsers;
  };

  function getData(){
    return resultData;
  };

  return {
    loadUserData : loadUserData,
    getData: getData
  }
})

.factory('langSvc', function() {

  var outLang = "en";

  function selectLang(selLang) {
    outLang = selLang;
  }

  function getLang() {
    return outLang;
  }

  return {
    setLang: selectLang,
    getLang: getLang
  }
})

.factory('currTranslateSvc',function(){
    
    var data;
    
    function setData(data){
        this.data = data;
    }
    
    function getData(){
        return this.data;
    }
    
    return {
        setData: setData,
        getData: getData
    }
})

.factory('newsStoreSvc',function(){
    
    var data;
    
    function setData(data){
        this.data = data;
    }
    
    function getData(){
        return this.data;
    }
    
    return {
        setData: setData,
        getData: getData
    }
})

.factory('SSMOffices', function() {
  var offices = [
      {
      "id": 0,
           "placeHolderName":"",
      "name" : "Head Office",
      "nameMs" : "Ibu Pejabat",
      "address" : "<br>Menara SSM@Sentral,<br>No. 7, Jalan Stesen Sentral 5,<br>Kuala Lumpur Sentral,<br>50623 Kuala Lumpur, Malaysia.",
      "tel" : "03-7721 4000",
      "mainTel" : "03-7721 4000",
      "fax" : "03-7721 4001",
      "email" : "enquiry@ssm.com.my",
      "map" : "",
          "operationHourMs":"Hari : Isnin – Jumaat<br><br>Kaunter Perkhidmatan:<br>8.15 pagi - 4.15 petang",
          "operationHour":"Day : Monday – Friday<br><br>Service Counter:<br>8.15 am - 4.15 pm",
      "location":{
              "lat":3.1331544,
              "long":101.6838128
        }
    },
    {
      "id": 1,
        "placeHolderName":"",
      "name" : "JOHOR",
      "nameMs" : "JOHOR",
      "address" : "<br>Podium 4B & 5, Menara Ansar,<br>65 Jalan Trus,<br>80000 Johor Bahru, Johor.",
      "tel" : "07 224 4710",
        "mainTel" : "07 224 4710",
      "fax" : "07 224 1714",
      "email" : "hairul@ssm.com.my",
      "map" : "https://goo.gl/maps/Y5rdS",
        "operationHourMs":"Hari : Ahad – Khamis<br><br>Kaunter Perkhidmatan:<br>8.15 pagi - 4.00 petang",
        "operationHour":"Day : Sunday – Thursday<br><br>Service Counter:<br>8.15 am - 4.00 pm",
      "location" : {
        "lat" : 1.46627777777778,
        "long" : 103.75933333333333
      }
    },
    {
      "id": 2,
        "placeHolderName":"",
      "name" : "KEDAH",
       "nameMs" : "KEDAH",
      "address" : "<br>Tingkat 1 & 2,<br>Wisma PERKESO,<br>No.186 Jalan Teluk WanJah,<br>05538 Alor Setar, Kedah.",
      "tel" : "04-733 0111",
        "mainTel" : "04-733 0111",
      "fax" : "04-731 5517",
      "email" : "abrazak@ssm.com.my",
      "map" : "https://goo.gl/maps/8RHJc",
       "operationHourMs":"Hari : Ahad – Khamis<br><br>Kaunter Perkhidmatan:<br>8.15 pagi - 4.00 petang (Ahad - Rabu)<br>8.15 pagi - 3.45 petang (Khamis)",
"operationHour":"Day : Sunday – Thursday<br><br>Service Counter:<br>8.15 am - 4.00 pm (Sunday - Wednesday)<br>8.15 am - 3.45 pm (Thursday)",
      "location" : {
        "lat" : 6.12336111111111,
        "long" : 100.37327777777778
      }
    },
    {
      "id": 3,
        "placeHolderName":"",
      "name" : "KELANTAN",
        "nameMs" : "KELANTAN",
      "address" : "<br>Tingkat 2, 3 & 4,<br>Kota Sri Mutiara,<br>Jalan Sultan Yahya Petra,<br>15150 Kota Bharu, Kelantan.",
      "tel" : "09-748 2860 / 09-748 4599",
        "mainTel" : "04-731 5517",
      "fax" : "04-731 5517",
      "email" : "wrahim@ssm.com.my",
      "map" : "https://goo.gl/maps/5N5Xh",
        "operationHourMs":"Hari : Ahad – Khamis<br><br>Kaunter Perkhidmatan:<br>8.15 pagi - 4.00 petang (Ahad - Rabu)<br>8.15 pagi – 3.45 petang (Khamis)",
"operationHour":"Day : Sunday – Thursday<br><br>Service Counter:<br>8.15 am - 4.00 pm (Sunday - Wednesday)<br>8.15 am - 3.45 pm (Thursday)",
      "location" : {
        "lat" : 6.113417,
        "long" : 102.249694
      }
    },
    {
      "id": 4,
        "placeHolderName":"",
      "name" : "MELAKA",
      "nameMs" : "MELAKA",
      "address" : "<br>Aras Bawah Menara MITC,<br>Kompleks MITC,<br>Jalan Konvensyen,<br>75450 Ayer Keroh, Melaka.",
      "tel" : "06-231 1717",
        "mainTel" : "06-231 1717",
      "fax" : "06-231 3502",
      "email" : "pauziah@ssm.com.my",
      "map" : "https://goo.gl/maps/1bmpL",
       "operationHourMs":"Hari : Isnin – Jumaat<br><br>Kaunter Perkhidmatan:<br>8.15 pagi - 4.15 petang",
        "operationHour":"Day : Monday – Friday<br><br>Service Counter:<br>8.15 am - 4.15 pm",
      "location" : {
        "lat" : 2.271389,
        "long" : 102.286972
      }
    },
    {
      "id": 5,
        "placeHolderName":"",
      "name" : "NEGERI SEMBILAN",
       "nameMs" : "NEGERI SEMBILAN",
      "address" : "<br>Aras 3,<br>Wisma Perkeso,<br>Lot 3757 & 52,<br>Jalan Sungai Ujong,<br>70000 Seremban, Negeri Sembilan.",
      "tel" : "06-761 9098/5506, 764 1885/5598",
        "mainTel" : "06-761 9098",
      "fax" : "06-765 5877",
      "email" : "krudin@ssm.com.my",
      "map" : "https://goo.gl/maps/6yLYo",
        "operationHourMs":"Hari : Isnin – Jumaat<br><br>Kaunter Perkhidmatan:<br>8.15 pagi - 4.15 petang",
        "operationHour":"Day : Monday – Friday<br><br>Service Counter:<br>8.15 am - 4.15 pm",
      "location" : {
        "lat" : 2.7214183,
        "long" : 101.939196
          
      }
    },
    {
      "id": 6,
        "placeHolderName":"",
      "name" : "PAHANG",
        "nameMs" : "PAHANG",
      "address" : "<br>No. 3.04 – 3.11,<br>Tingkat 3,<br>Mahkota Square,<br>Jalan Mahkota,<br>25000 Kuantan, Pahang.",
      "tel" : "09-516 4866 / 09-516 4600",
        "mainTel" : "09-516 4866",
      "fax" : "09-516 1869 / 09-516 3316",
      "email" : "roslina@ssm.com.my",
      "map" : "https://goo.gl/maps/AWkPJ",
        "operationHourMs":"Hari : Isnin – Jumaat<br><br>Kaunter Perkhidmatan:<br>8.15 pagi - 4.15 petang",
        "operationHour":"Day : Monday – Friday<br><br>Service Counter:<br>8.15 am - 4.15 pm",
      "location" : {
        "lat" : 3.807472,
        "long" : 103.328361
      }
    },
    {
      "id": 7,
        "placeHolderName":"",
      "name" : "PERAK",
        "nameMs" : "PERAK",
      "address" : "<br>Tingkat 2C,<br>Angsana Ipoh Mall,<br>Jalan Hospital,<br>30450 Ipoh, Perak.",
      "tel" : "05-241 6900 / 05-254 7913 / 05-253 3071",
        "mainTel" : "05-241 6900",
      "fax" : "05-255 7162",
      "email" : "rozaini@ssm.com.my",
      "map" : "https://goo.gl/maps/lj0EH",
        "operationHourMs":"Hari : Isnin – Jumaat<br><br>Kaunter Perkhidmatan:<br>8.15 pagi - 4.15 petang",
        "operationHour":"Day : Monday – Friday<br><br>Service Counter:<br>8.15 am - 4.15 pm",
      "location" : {
        "lat" : 4.602833,
        "long" : 101.094889
      }
    },
    {
      "id": 8,
        "placeHolderName":"",
      "name" : "PERLIS",
        "nameMs" : "PERLIS",
      "address" : "<br>Tingkat 8,<br>Bangunan KWSP,<br>Jalan Bukit Lagi,<br>01000 Kangar, Perlis.",
      "tel" : "04-978 1877 / 04-976 7899 / 04-977 3182",
        "mainTel" : "04-978 1877",
      "fax" : "04-977 4758",
      "email" : "lramle@ssm.com.my",
      "map" : "https://goo.gl/maps/hzWSu",
        "operationHourMs":"Hari : Isnin – Jumaat<br><br>Kaunter Perkhidmatan:<br>8.15 pagi - 4.15 petang",
        "operationHour":"Day : Monday – Friday<br><br>Service Counter:<br>8.15 am - 4.15 pm",
      "location" : {
        "lat" : 6.435556,
        "long" : 100.193083
      }
    },
    {
      "id": 9,
        "placeHolderName":"",
      "name" : "PULAU PINANG",
        "nameMs" : "PULAU PINANG",
      "address" : "<br>Tingkat 6-7,<br>Bangunan KWSP,<br>No. 3009, Off Leboh Tenggiri 2,<br>Bandar Seberang Jaya,<br>13700 Pulau Pinang.",
      "tel" : "04-397 7793",
        "mainTel" : "04-397 779",
      "fax" : "04-397 7713",
      "email" : "mnasir@ssm.com.my",
      "map" : "https://goo.gl/maps/YI2kz",
        "operationHourMs":"Hari : Isnin – Jumaat<br><br>Kaunter Perkhidmatan:<br>8.15 pagi - 4.15 petang",
        "operationHour":"Day : Monday – Friday<br><br>Service Counter:<br>8.15 am - 4.15 pm",
      "location" : {
        "lat" : 5.394222,
        "long" : 100.397500
      }
    },
    {
      "id": 10,
        "placeHolderName":"",
      "name" : "SABAH",
        "nameMs" : "SABAH",
      "address" : "<br>Tingkat 4,<br>Menara MAA,<br>6 Lorong Api-Api,<br>Locked Bag 2039,<br>88999 Kota Kinabalu, Sabah.",
      "tel" : "088-233 551 / 088-233 346",
        "mainTel" : "088-233 551",
      "fax" : "088-237 884",
      "email" : "yasin@ssm.com.my",
      "map" : "https://goo.gl/maps/nSp8C",
        "operationHourMs":"Hari : Isnin – Jumaat<br><br>Kaunter Perkhidmatan:<br>8.00 pagi - 4.00 petang",
        "operationHour":"Day : Monday – Friday<br><br>Service Counter:<br>8.00 am - 4.00 pm",
      "location" : {
        "lat" : 5.973861,
        "long" : 116.069361
      }
    },
    {
      "id": 11,
        "placeHolderName":"",
      "name" : "SARAWAK",
        "nameMs" : "SARAWAK",
      "address" : "<br>Tingkat 2 & 3,<br>Wisma STA,<br>26 Jalan Datuk Abang Abdul Rahim,<br>93450 Kuching, Sarawak.",
      "tel" : "082-481 499",
        "mainTel" : "082-481 499",
      "fax" : "082-334 622",
      "email" : "adzman@ssm.com.my",
      "map" : "https://goo.gl/maps/u7tPI",
        "operationHourMs":"Hari : Isnin – Jumaat<br><br>Kaunter Perkhidmatan:<br>8.00 pagi - 4.00 petang<br>Rehat : 1.00 tengahari - 2.00 petang",
        "operationHour":"Day : Monday – Friday<br><br>Service Counter:<br>8.00 am - 4.00 pm<br>Break : 1.00 pm - 2.00 pm",
      "location" : {
        "lat" : 1.552139,
        "long" : 110.368917
      }
    },
    {
      "id": 12,
        "placeHolderName":"",
      "name" : "SELANGOR",
        "nameMs" : "SELANGOR",
      "address" : "<br>Tingkat 3,<br>Plaza Alam Sentral,<br>Jalan Majlis, Seksyen 14,<br>40000 Shah Alam, Selangor.",
      "tel" : "03-5511 6500 / 03-5513 5997",
        "mainTel" : "03-5511 6500",
      "fax" : "03-5510 4200",
      "email" : "lokman@ssm.com.my",
      "map" : "https://goo.gl/maps/YLudk",
        "operationHourMs":"Hari : Isnin – Jumaat<br><br>Kaunter Perkhidmatan:<br>8.15 pagi - 4.15 petang",
        "operationHour":"Day : Monday – Friday<br><br>Service Counter:<br>8.15 am - 4.15 pm",
      "location" : {
        "lat" : 3.073583,
        "long" : 101.516972
      }
    },
    {
      "id": 13,
        "placeHolderName":"",
      "name" : "TERENGGANU",
        "nameMs" : "TERENGGANU",
      "address" : "<br>Tingkat 6-7,<br>Menara Yayasan Islam,<br>Jalan Sultan Omar,<br>20300 Kuala Terengganu, Terengganu.",
      "tel" : "09-623 7170 / 09-624 0721",
        "mainTel" : "09-623 7170",
      "fax" : "09-623 0945",
      "email" : "kmrudin@ssm.com.my",
      "map" : "https://goo.gl/maps/4VJHQ",
//        "operationHourMs":"",
//        "operationHour":"",
      "location" : {
        "lat" : 5.325972,
        "long" : 103.141611
      }
    },
    {
      "id": 14,
        "placeHolderName":"",
      "name" : "W.P. LABUAN",
        "nameMs" : "W.P. LABUAN",
      "address" : "<br>No. 6A1 & 6A2,<br>Tingkat 6, Block 4,<br>Kompleks Ujana Kewangan,<br>Jalan Merdeka,<br>87000 W.P. Labuan.",
      "tel" : "06-231 1717",
        "mainTel" : "06-231 1717",
      "fax" : "06-231 3502",
      "email" : "pauziah@ssm.com.my",
      "map" : "https://goo.gl/maps/1bmpL",
        "operationHourMs":"Hari : Isnin – Jumaat<br><br>Kaunter Perkhidmatan:<br>8.00 pagi - 4.00 petang",
        "operationHour":"Day : Monday – Friday<br><br>Service Counter:<br>8.00 am - 4.00 pm",
      "location" : {
        "lat" : 2.271389,
        "long" : 102.286972
      }
    },
    {
      "id": 15,
        "placeHolderName":"",
      "name" : "BRANCH: MIRI",
        "nameMs" : "CAWANGAN: MIRI",
      "address" : "<br>Ground Floor,<br>Yu Lan Plaza, Lot 97,<br>Jalan Brooke,<br>98000 Miri, Sarawak.",
      "tel" : "085-424777 / 421117",
        "mainTel" : "085-424777",
      "fax" : "085-422225",
      "email" : "ronatkinson@ssm.com.my",
      "map" : "https://goo.gl/maps/bAeQS",
       "operationHourMs":"Hari : Isnin – Khamis<br><br>Kaunter Perkhidmatan:<br>8.00 pagi - 4.00 petang<br>Hari : Jumaat<br><br>Kaunter Perkhidmatan:<br>8.00 pagi - 4.00 petang<br>Rehat : 1.00 tengahari - 2.00 petang",
"operationHour":"Day : Monday – Thursday<br><br>Service Counter:<br>8.15 am - 4.00 pm<br>Day : Friday<br><br>Service Counter:<br>8.00 am - 4.00 pm<br>Break : 1.00 pm - 2.00 pm",
      "location" : {
        "lat" : 4.393694,
        "long" : 113.987861
      }
    },
    {
      "id": 16,
        "placeHolderName":"",
      "name" : "BRANCH: MUAR",
        "nameMs" : "CAWANGAN: MUAR",
      "address" : "<br>No.1, Jalan Perdagangan,<br>Off Bulatan Jalan Bakri,<br>84000 Muar, Johor.",
      "tel" : "06-9546611 / 9547711",
        "mainTel" : "06-9546611",
      "fax" : "06-9546600",
      "email" : "norsilawati@ssm.com.my",
      "map" : "https://goo.gl/maps/bAeQS",
        "operationHourMs":"Hari : Ahad – Khamis<br><br>Kaunter Perkhidmatan:<br>8.15 pagi - 4.00 petang",
        "operationHour":"Day : Sunday – Thursday<br><br>Service Counter:<br>8.15 am - 4.00 pm",
      "location" : {
        "lat" : 2.048778,
        "long" : 102.568806
      }
    },
    {
      "id": 17,
        "placeHolderName":"",
      "name" : "BRANCH: TAWAU",
        "nameMs" : "CAWANGAN: TAWAU",
      "address" : "<br>TB 4444, GF & 1st Floor,<br>Blok F, Sabindo Square<br>Jalan Dunlop,<br>91018 Tawau, Sabah.",
      "tel" : "089-750 585 / 752 585",
        "mainTel" : "089-750 585",
      "fax" : "089-754 585",
      "email" : "N/A",
      "map" : "https://goo.gl/maps/YA9tv",
        "operationHourMs":"Hari : Isnin – Jumaat<br><br>Kaunter Perkhidmatan:<br>8.00 pagi - 4.00 petang",
        "operationHour":"Day : Monday – Friday<br><br>Service Counter:<br>8.00 am - 4.00 pm",
      "location" : {
        "lat" : 4.243917,
        "long" : 117.893333
      }
    },
    {
      "id": 18,
        "placeHolderName":"",
      "name" : "BRANCH: TEMERLOH",
        "nameMs" : "CAWANGAN: TEMERLOH",
      "address" : "<br>No. 15 & 16,<br>Jalan Ahmad Shah,<br>28000 Temerloh, Pahang.",
      "tel" : "09-2964600 / 2965600",
        "mainTel" : "09-2964600",
      "fax" : "09-2960010",
      "email" : "adnan@ssm.com.my",
      "map" : "https://goo.gl/maps/B7WUuCJearo",
        "operationHourMs":"Hari : Isnin – Jumaat<br><br>Kaunter Perkhidmatan:<br>8.15 pagi - 4.15 petang",
        "operationHour":"Day : Monday – Friday<br><br>Service Counter:<br>8.15 am - 4.15 pm",
      "location" : {
        "lat" : 4.393694,
        "long" : 113.987861
      }
    },
   {
      "id": 19,
       "placeHolderName":"",
      "name" : "UTC: KUALA LUMPUR",
       "nameMs" : "UTC: KUALA LUMPUR",
      "address" : "<br>Suruhanjaya Syarikat Malaysia,<br>Ruang Legar, Tingkat Bawah,<br>Bangunan UTC Kuala Lumpur,<br>Jalan Pudu (Pudu Sentral),<br>55100 Kuala Lumpur.",
      "tel" : "03-2026 2041 / 03-2026 2042",
       "mainTel" : "03-2026 2041",
      "fax" : "03-2026 2036",
      "map" : "https://goo.gl/maps/ln8YY",
       "operationHourMs":"Hari : Isnin – Ahad (kecuali cuti Umum)<br><br>Kaunter Perkhidmatan:<br>8.30 pagi - 6.00 petang<br>8.00 malam – 9.00 malam<br>Rehat : 7.00 malam – 8.00 malam<br>Rehat (Jumaat) : 12.15 tengahari – 2.45 petang",
"operationHour":"Day : Monday – Sunday (except Public Holiday)<br><br>Service Counter:<br><br>8.30 am - 6.00 pm<br>8.00 pm – 9.00 pm<br>Break : 7.00 pm – 8.00 pm<br>Break (Friday) : 12.15 pm – 2.45 pm",
      "location" : {
        "lat" : 3.145694,
        "long" : 101.700778
      }
    },
     {
      "id": 20,
         "placeHolderName":"",
      "name" : "UTC: MELAKA",
         "nameMs" : "UTC: MELAKA",
      "address" : "<br>Aras 3, Wisma UTC,<br>Jalan Hang Tuah,<br>75300, Melaka.",
      "tel" : "06-2830076",
         "mainTel" : "06-2830076",
      "fax" : "N/A",
      "map" : "https://goo.gl/maps/bDwMn",
         "operationHourMs":"Hari : Isnin - Ahad (kecuali cuti Umum / Negeri Melaka)<br><br>Kaunter Perkhidmatan:<br>Ahad - Khamis<br>8.30 pagi - 1.00 tenghari<br>Rehat : 1.00 tengahari – 2.00 petang<br>2.00 petang – 4.30 petang<br>Rehat : 4.30 petang – 5.30 petang<br>5.30 petang – 7.00 malam<br>Rehat : 7.00 malam – 8.00 malam<br>8.00 malam – 9.30 malam<br>Jumaat<br>8.30 pagi  - 12.15 tengahari<br>Rehat : 12.15 tengahari – 2.45 petang<br>2.45 petang – 4.30 petang<br>Rehat : 4.30 petang – 5.30 petang<br>5.30 petang – 7.00 malam<br>Rehat : 7.00 malam – 8.00 malam<br>8.00 malam – 9.30 malam",
"operationHour":"Day : Monday - Sunday (except Public Holiday / Negeri Melaka)<br><br>Service Counter:<br><br>Sunday - Thursday<br>8.30 am - 1.00 pm<br>Break : 1.00 pm – 2.00 pm<br>2.00 pm – 4.30 pm<br>Break : 4.30 pm – 5.30 pm<br>5.30 pm – 7.00 pm<br>Break : 7.00 pm – 8.00 pm<br>8.00 pm – 9.30 pm<br>Friday<br>8.30 am  - 12.15 pm<br>Break : 12.15 pm – 2.45 pm<br>2.45 pm – 4.30 pm<br>Break : 4.30 pm – 5.30 pm<br>5.30 pm – 7.00 pm<br>Break : 7.00 pm – 8.00 pm<br>8.00 pm – 9.30 pm",
      "location" : {
        "lat" : 2.204250,
        "long" : 102.246472
      }
    },
    {
      "id": 21,
        "placeHolderName":"",
      "name" : "UTC: PAHANG",
        "nameMs" : "UTC: PAHANG",
      "address" : "<br>1-11 Aras Satu,<br>Bangunan UTC Pahang,<br>Jalan Stadium,<br>25200 Kuantan, Pahang.",
      "tel" : "09-5124137",
        "mainTel" : "09-5124137",
      "fax" : "09-5124136",
      "map" : "https://goo.gl/maps/hWeza",
        "operationHourMs":"Hari : Isnin - Ahad (kecuali cuti Umum / Negeri Pahang)<br><br>Kaunter Perkhidmatan:<br>8.30 pagi - 10.00 malam<br>Rehat : 7.00 malam – 8.00 malam",
"operationHour":"Day : Monday – Friday (except Public Holiday / Negeri Pahang)<br><br>Service Counter:<br>8.30 am - 10.00 pm<br>Break : 7.00 pm – 8.00 pm",
      "location" : {
        "lat" : 3.812611,
        "long" : 103.323472
      }
    },
//    {
//      "id": 22,
//       "placeHolderName":"",
//      "name" : "UTC: KEDAH",
//        "nameMs" : "UTC: KEDAH",
//      "address" : "<br>No 4-7, Kompleks MBAS,<br>Jalan Kolam Air,<br>05675 Alor Setar, Kedah.",
//      "tel" : "04-736 0815",
//        "mainTel" : "04-736 0815",
//      "fax" : " 04-732 0622",
//      "map" : "https://goo.gl/maps/C89y3",
//        "operationHourMs":"",
//        "operationHour":"",
//      "location" : {
//        "lat" : 6.118222,
//        "long" : 100.369778
//      }
//    },
    {
      "id": 23,
      "placeHolderName":"",
      "name" : "UTC: TERENGGANU",
        "nameMs" : "UTC: TERENGGANU",
      "address" : "<br>Suruhanjaya Syarikat Malaysia,<br>L4-UTC-04, UTC Terengganu,<br>Paya Bunga Square,<br>Jalan Masjid Abidin,<br>21000 Kuala Terengganu, Terengganu.",
      "tel" : "09-622 6174 / 09-622 6765",
        "mainTel" : "09-622 6174",
      "fax" : "09-622 6765",
      "map" : "https://goo.gl/maps/aRLv2nqQSY32",
        "operationHourMs":"Hari : Isnin - Ahad (kecuali cuti Umum / Negeri Terengganu)<br><br>Kaunter Perkhidmatan:<br>8.30 pagi - 10.00 malam<br>Rehat : 7.00 malam – 8.00 malam",
"operationHour":"Day : Monday – Friday (except Public Holiday / Negeri Terengganu)<br><br>Service Counter:<br>8.30 am - 10.00 pm<br>Break : 7.00 pm – 8.00 pm",
      "location" : {
        "lat" : 5.3310387,
        "long" : 103.1362114
      }
  },
  {
      "id": 24,
      "placeHolderName":"",
      "name" : "SERVICE CENTRE: LANGKAWI",
      "nameMs" : "PUSAT SERVIS: LANGKAWI",
      "address" : "<br>Pusat Perkhidmatan,<br>Suruhanjaya Syarikat Malaysia,<br>No 4, Aras Mezzanine,<br>Kompleks LADA,<br>Jalan Persiaran Putra,<br>07000 Langkawi, Kedah.",
      "tel" : "04-966 7943",
      "mainTel" : "04-966 7943",
      "fax" : "04-966 5318",
      "map" : "https://goo.gl/maps/RrdkR",
      "operationHourMs":"Pusat Perkhidmatan Kaunter :<br>Rabu : 10.00 pagi – 4.00 petang<br>Rehat : 1.00 tengahari – 2.00 petang<br>Khamis : 8.15 pagi – 3.45  petang<br>Rehat : 1.00 tengahari – 2.00 petang",
"operationHour":"Service Counter:<br><br>Wednesday : 10.00 am – 4.00 pm<br>Break : 1.00 pm – 2.00 pm<br>Thursday : 8.15 am – 3.45 pm<br>Break : 1.00 pm – 2.00 pm",
      "location" : {
        "lat" : 6.311528,
        "long" : 99.855972
      }
  },
  {
      "id": 25,
      "placeHolderName":"",
      "name" : "BSS REG SERVICE COUNTER: PUTRAJAYA",
      "nameMs" : "KAUNTER PENDAFTARAN SERVIS BBS: PUTRAJAYA",
      "address" : "<br>Kaunter Khidmat Pengguna, Aras G,<br>Kementerian Perdagangan Dalam<br>Negeri Koperasi & Kepenggunaan<br>(KPDNKK),<br>No. 13, Persiaran Perdana,<br>Presint 2,<br>Pusat Pentadbiran Kerajaan Persekutuan,<br>62623 Putrajaya, W.P. Putrajaya.",
      "tel" : "03-8882 6509 / 03-8882 6480",
      "mainTel" : "03-8882 6509",
      "fax" : "N/A",
      "map" : "https://goo.gl/maps/KF02H",
      "operationHourMs":"Hari : Isnin – Jumaat<br><br>Kaunter Perkhidmatan:<br>8.00 pagi - 4.00 petang<br>Rehat(Jumaat) : 12.15 tengahari – 2.45 petang",
"operationHour":"Day : Monday – Friday<br><br>Service Counter:<br>8.00 am - 4.00 pm<br>Break (Friday) : 12.15 pm – 2.45 pm",
      "location" : {
        "lat" : 2.920500,
        "long" : 101.687472
      }
  },
  {
      "id": 26,
      "placeHolderName":"",
      "name" : "CYBERJAYA",
      "nameMs" : "CYBERJAYA",
      "address" : "<br>Unit LG-03, SME Technopreneur Centre 3,<br>Blok 3740, Persiaran APEC, Cyer 8,<br>63500 Cyberjaya,<br>Selangor.",
      "tel" : "03-83180281",
      "mainTel" : "03-83180281",
      "fax" : "03-83180352",
      "map" : "",
      "operationHourMs":"Hari : Selasa, Rabu dan Khamis<br>Pejabat : 8.15 pagi - 5.15 petang<br><br>Kaunter Perkhidmatan:<br>8.15 pagi - 4.00 petang<br>Rehat : 1.00 petang – 2.00 petang",
"operationHour":"Day : Tuesday, Wednesday and Thursday<br>Office : 8.15 am - 5.15 pm<br><br>Service Counter:<br>8.15 am - 4.00 pm<br>Break : 1.00 pm – 2.00 pm",
      "location" : {
        "lat" : 2.9084165,
        "long" : 101.6376571
      }
  }
  ];

  function getList() {
    return offices;
  }
    


  return {
    list : getList
  }

});

